import pandas as pd
import numpy as np
import random

class Environment():
    def __init__(self, data, normalize=True, starting_time_step=96+8+1, normalization_n=96, c=1.0, spread=0.00001):

        self.data = data
        self.normalize = normalize
        self.normalization_n = normalization_n # -1 for 0-indexing
        self.spread = spread
        self.starting_time_step = starting_time_step
        self.time_step = self.starting_time_step # how far is the environment in the time-series.
        self.v_t = 1.0
        self.c = c # proportion of portfolio to invest at each time step
        self.current_position = 0

        self.state = self.process_features(time_step=self.time_step)
        self.nb_features = len(self.state)

    def process_features(self, time_step):

        # time features
        time_features = []
        tstamp = self.data.date.iloc[time_step]
        time_features.append(np.sin(2*np.pi*(tstamp.minute/60)))
        time_features.append(np.sin(2*np.pi*(tstamp.hour/24)))
        time_features.append(np.sin(2*np.pi*(tstamp.weekday()/7)))

        opens = []
        highs = []
        lows = []
        closes = []
        volumes = []

        for c_ix in np.arange(1, len(self.data.columns), 5):
            for j in range(8):

                temp = self.data.iloc[time_step-j-self.normalization_n:time_step-j+1, c_ix+0]
                log_rets = np.log(temp) - np.log(temp.shift(1))
                log_rets = log_rets[1:].tolist()
                opens.append((log_rets[-1]-np.mean(log_rets))/np.std(log_rets))

                temp = self.data.iloc[time_step-j-self.normalization_n:time_step-j+1, c_ix+1]
                log_rets = np.log(temp) - np.log(temp.shift(1))[1:]
                log_rets = log_rets[1:].tolist()
                highs.append((log_rets[-1]-np.mean(log_rets))/np.std(log_rets))

                temp = self.data.iloc[time_step-j-self.normalization_n:time_step-j+1, c_ix+2]
                log_rets = np.log(temp) - np.log(temp.shift(1))[1:]
                log_rets = log_rets[1:].tolist()
                lows.append((log_rets[-1]-np.mean(log_rets))/np.std(log_rets))

                temp = self.data.iloc[time_step-j-self.normalization_n:time_step-j+1, c_ix+3]
                log_rets = np.log(temp) - np.log(temp.shift(1))[1:]
                log_rets = log_rets[1:].tolist()
                closes.append((log_rets[-1]-np.mean(log_rets))/np.std(log_rets))

                temp = self.data.iloc[time_step-j-self.normalization_n:time_step-j+1, c_ix+4]
                log_rets = np.log(temp) - np.log(temp.shift(1))[1:]
                log_rets = log_rets[1:].tolist()
                volumes.append((log_rets[-1]-np.mean(log_rets))/np.std(log_rets))

        opens = [np.clip(x, -10, 10) for x in opens]
        highs = [np.clip(x, -10, 10) for x in highs]
        lows = [np.clip(x, -10, 10) for x in lows]
        closes = [np.clip(x, -10, 10) for x in closes]
        volumes = [np.clip(x, -10, 10) for x in volumes]

        position_features = [0, 0, 0]
        if self.current_position == -1:
            position_features[0] = 1
            position_features[1] = 0
            position_features[2] = 0
        elif self.current_position == 0:
            position_features[0] = 0
            position_features[1] = 1
            position_features[2] = 0
        elif self.current_position == 1:
            position_features[0] = 0
            position_features[1] = 0
            position_features[2] = 1
        else:
            print("What sort of position do you have?!")

        state = time_features + opens + highs + lows + closes + volumes + position_features

        #time_features = self.data.iloc[time_step, 0:3].tolist()
        #market_features = self.data.iloc[time_step, 3:-3].tolist()
        #position_features = self.data.iloc[time_step, -3:].tolist()

        #state = time_features + market_features + position_features

        return state

    def reset(self):

        self.time_step = self.starting_time_step
        self.v_t = 1.0
        self.current_position = 0
        self.state = self.process_features(time_step=self.time_step)

        return self.state

    def step(self, action, augment_action=False):

        # NB: CHANGE THE STUFF BELOW!!
        v_t = self.v_t + action * self.c * (self.data.iloc[self.time_step+1, 4] - self.data.iloc[self.time_step+1, 1])
        #v_t = self.v_t + action * self.c * random.uniform(-0.01, 0.01)

        if self.state[-3] == 1:

            self.current_position = -1

        elif self.state[-2] == 1:

            self.current_position = 0

        elif self.state[-1] == 1:

            self.current_position = 1

        else:
            #print("YOU HAVE A BUG IN THE POSITION FEATURE!")
            pass

        v_t -= self.c * np.abs(action-self.current_position) * self.spread

        #print("v_t: ", v_t)

        reward = np.log(v_t/self.v_t)

        if not augment_action:
            self.v_t = v_t
            self.current_position = action
            self.time_step += 1
            self.state = self.process_features(time_step=self.time_step)

        if (self.time_step + 1) >= len(self.data):
            dead = True
        else:
            dead = False

        return self.state, reward, dead


