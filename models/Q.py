import torch
import torch.nn as nn
import torch.nn.functional as F

class Q(nn.Module):
    def __init__(self, device, nb_features=198, h_size=256):
        super(Q, self).__init__()

        self.device = device
        self.nb_features = nb_features
        self.h_size = h_size

        # The network receives a frame from the game
        self.mlp1 = nn.Linear(self.nb_features, h_size)
        self.mlp2 = nn.Linear(h_size, h_size)

        # We take the output from the final convolutional layer and send it to a recurrent layer.
        # The input must be reshaped into [batch, trace, units] for rnn processing,
        # and then returned to [batch, units] when sent through the upper levels.
        self.lstm = nn.LSTM(self.h_size, self.h_size)

        self.AW = nn.Parameter(torch.rand(int(self.h_size/2), 3))
        self.VM = nn.Parameter(torch.rand(int(self.h_size/2), 1))

    def forward(self, input, h, c, trace=False, batch_size=32, trace_length=96):

        input = input.to(self.device)
        h = h.to(self.device)
        c = c.to(self.device)

        #print(input.shape)
        out = F.elu(self.mlp1(input))
        #print(out.shape)
        out = F.elu(self.mlp2(out))
        #print(out.shape)

        if trace:

            out = out.view(batch_size, trace_length, out.size()[1]).transpose(0, 1)

            out, (h, c) = self.lstm(out, (h, c))

            h1, c1 = h, c
            out = out.transpose(0, 1).contiguous()
            out = out.view(-1, out.size()[2])

        else:

            out = out.unsqueeze(dim=0)

            out, (h1, c1) = self.lstm(out, (h, c))

            out = out[0, :, :]

        # The output from the recurrent player is then split into separate Value and Advantage streams
        streamA = out[:, :int(out.size()[1]/2)] # trace==False -> 1x256
        streamV = out[:, int(out.size()[1]/2):] # trace==False -> 1x256

        Advantage = streamA @ self.AW # trace==False: 1x3
        Value = streamV @ self.VM # trace==False: 1x1

        # Then combine them together to get our final Q-values
        Qout = Value + (Advantage - torch.mean(Advantage, dim=1).unsqueeze(dim=-1))

        predict = torch.argmax(Qout, dim=1)

        return Qout, predict, (h1, c1)
