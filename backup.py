import pandas as pd
import numpy as np
import random
import torch
import os

from helper import *

from environment import Environment

from models.Q import Q

data = pd.read_csv("./data/data.csv")
data_pre = data.iloc[:480+96+1, :]

env = Environment(data=data_pre)

env_online = Environment(data=data)

# Setting the training parameters
buffer_size = 100 #480
batch_size = 4 # How many experience traces to use for each training step.
trace_length = 96 # How long each experience trace will be when training
update_freq = 96 # How often to perform a training step.
y = .99 # Discount factor on the target Q-values
num_episodes = 1000 # How many episodes of game environment to train network with.
load_model = False # Whether to load a saved model.
path = "./drqn" # The path to save our model to.
h_size = 256 # The size of the final convolutional layer before splitting it into Advantage and Value streams.
tau = 0.001
summaryLength = 10

device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

# These classes allow us to store experies and sample then randomly to train the network. Episode buffer stores
# experiences for each individal episode. Experience buffer stores entire episodes of experience, and sample()
# allows us to get training batches needed from the network.

class experience_buffer():
    def __init__(self, buffer_size=480):
        self.buffer = []
        self.buffer_size = buffer_size

    def add(self, experience):
        if len(self.buffer) + 1 >= self.buffer_size:
            # if len==999 then nothing is added as self.buffer[0:0] is not defined.
            # if len==1000 then buffer[0:1] = [] removed the first element.
            # weird way of popping stuff...
            self.buffer[0:(1 + len(self.buffer)) - self.buffer_size] = []
        self.buffer.append(experience)

    def sample(self, batch_size, trace_length):
        sampled_episodes = random.sample(self.buffer, batch_size)
        sampledTraces = []
        for episode in sampled_episodes:
            point = np.random.randint(0, len(episode) + 1 - trace_length)
            sampledTraces.append(episode[point:point + trace_length])
        sampledTraces = np.array(sampledTraces)
        return np.reshape(sampledTraces, [batch_size * trace_length, 5])

mainQN = Q(device=device, h_size=h_size).to(device)
targetQN = Q(device=device, h_size=h_size).to(device)

optimizer = torch.optim.Adam(mainQN.parameters(), lr=0.00025)

myBuffer = experience_buffer(buffer_size=buffer_size)

# create lists to contain total rewards and steps per episode
jList = []
rList = []
total_steps = 0

# Make a path for our model to be saved in.
if not os.path.exists(path):
    os.makedirs(path)

# update target network
targetQN.load_state_dict(mainQN.state_dict())

targetQN.eval()

# ------------------------------------ PRE-TRAIN STEP --------------------------------------

episodeBuffer = []

# Reset environment and get first new observation
sP = env.reset()
s = torch.FloatTensor(sP).to(device).unsqueeze(dim=0)

d = False
rAll = 0
j = 0

while not d:

    mainQN.eval()
    j += 1

    # perform step for each action to do action augmentation

    s1P_short, r_short, d_short = env.step(action=-1, augment_action=True)
    s1P_hold, r_hold, d_hold = env.step(action=0, augment_action=True)
    s1P_long, r_long, d_long = env.step(action=1, augment_action=True)

    if r_hold > r_short:
        a = 0
        if r_long > r_hold:
            a = 1
    else:
        a = -1
        if r_long > r_short:
            a = 1

    s1P, r, d = env.step(action=a, augment_action=False)
    s1 = torch.FloatTensor(s1P).to(device).unsqueeze(dim=0)

    total_steps += 1
    episodeBuffer.append(np.reshape(np.array([s.cpu().detach().numpy(), a, r, s1.cpu().detach().numpy(), d]), [1, 5]))

    s = s1
    if d == True:
        myBuffer.add(episodeBuffer)
        break



while not d:

    if (total_steps % update_freq == 0) and (len(myBuffer.buffer) == buffer_size):

        #print("TRAINING")
        targetQN = updateTargetPytorch(targetQN, mainQN, tau)

        # Get a random batch of experiences
        trainBatch = myBuffer.sample(batch_size, trace_length)  # Get a random batch of experiences.

        # Reset the recurrent layer's hidden state
        state_train = (torch.FloatTensor(np.zeros([1, batch_size, h_size])).to(device),
                        torch.FloatTensor(np.zeros([1, batch_size, h_size])).to(device))

        # Below we perform the Double-DQN update to the target Q-values
        input = torch.FloatTensor(np.vstack(trainBatch[:, 3])).to(device)

        with torch.no_grad():

            Q1, a1, _ = mainQN(input=input, h=state_train[0], c=state_train[1], trace=True,
                               batch_size=batch_size, trace_length=trace_length)
            Q2, a2, _ = targetQN(input=input, h=state_train[0], c=state_train[1], trace=True,
                                 batch_size=batch_size, trace_length=trace_length)

            end_multipler = -(trainBatch[:, 4] - 1)

            #doubleQ = Q2[:, a1]
            doubleQ = Q2[torch.arange(Q2.size(0)).long(), a1]

            e0 = torch.FloatTensor(trainBatch[:, 2].astype("float")).to(device)
            e1 = torch.FloatTensor(end_multipler.astype("float")).to(device)
            targetQ = e0 + (y * doubleQ * e1)

        # Compute loss and train
        mainQN.train()
        optimizer.zero_grad()

        input = torch.FloatTensor(np.vstack(trainBatch[:, 0])).to(device)
        Q, a, _ = mainQN(input=input, h=state_train[0], c=state_train[1], trace=True,
                         batch_size=batch_size, trace_length=trace_length)
        actions = trainBatch[:, 1].astype("int")
        one_hot_actions = np.zeros((len(actions), 3))
        one_hot_actions[np.arange(len(actions)), actions] = 1
        one_hot_actions = torch.FloatTensor(one_hot_actions).to(device)

        temp1 = Q * one_hot_actions
        temp2 = torch.sum(temp1, dim=1)

        # In order to only propagate accurate gradients through the network, we will mask the first
        # half of the losses for each trace as per Lample & Chatlot 2016
        maskA = torch.zeros([batch_size, trace_length // 2]).to(device)
        maskB = torch.ones([batch_size, trace_length // 2]).to(device)
        mask = torch.cat([maskA, maskB], dim=1)
        mask = mask.view(-1)

        td_error = (targetQ.detach() - temp2)**2

        loss = torch.mean(td_error * mask)

        loss.backward()

        optimizer.step()



# Add the episode to the experience buffer
myBuffer.add(episodeBuffer)
jList.append(j)
rList.append(rAll)

if len(rList) % summaryLength == 0 and len(rList) != 0:
    print (i, total_steps, np.mean(rList[-summaryLength:]), action_sum)

print("Percent of successful episodes: ", str(sum(rList)/num_episodes) + "%")