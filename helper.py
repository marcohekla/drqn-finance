import numpy as np

# This is a simple function to reshape our game frames.
def processState(state1):
    return np.reshape(state1, [21168])

def updateTargetPytorch(target, main, tau):

    keys = target.state_dict().keys()

    target_params = [target.state_dict()[k] for k in keys]
    main_params = [main.state_dict()[k] for k in keys]

    new_params = [tau*pm + (1-tau)*pt for pm, pt in zip(main_params, target_params)]

    for i, k in enumerate(keys):
        target.state_dict()[k].data.copy_(new_params[i])

    return target
