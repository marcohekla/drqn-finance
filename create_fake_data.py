import pandas as pd
import numpy as np

n = 4*24*5*52*10 # 10 years, 52 weeks, 5 days a week, 24 hours, every 15 minutes
x = np.arange(n)
nb_features = 198

mu = 0
sigma = 0.001

def create_random_data(nb_data_points):

    e = np.random.normal(mu, sigma, nb_data_points)

    ts = [1]
    for i in range(len(e)):
        ts.append(ts[i]*(1+e[i]))

    return ts

data = []
for i in range(nb_features):
    data.append(create_random_data(nb_data_points=n))

data = pd.DataFrame(data)
data = data.transpose()

data.to_csv("../data/data.csv", index=False)
